#!/bin/bash

mkdir -p /root/.m2/
cp settings.xml ~/.m2/
./mvnw clean package

ret=$?
if [ $ret -ne 0 ];then
    echo "=====   build failed! ====="
    exit $ret
else
    echo -n "=====   build succeeded! ====="
fi

rm -rf output
mkdir output
# 拷贝Dockerfile及其需要的文件至output目录下
cp dockerfiles/* output/
mv target/*.jar output/
#mv spring-demo-*.jar output/